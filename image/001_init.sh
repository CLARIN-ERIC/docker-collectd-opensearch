#!/usr/bin/env bash

if [ "${COLLECTD_HOSTNAME}" != "" ]; then
  printf "Hostname override: %s\n" "${COLLECTD_HOSTNAME}"
  sed -i "s/#Hostname    \"localhost\"/Hostname    \"${COLLECTD_HOSTNAME}\"/g" /etc/collectd/collectd.conf
else
  printf "No hostname override (using %s)\n" "$(hostname)"
fi